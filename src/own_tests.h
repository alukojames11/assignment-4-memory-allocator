
#ifndef ASSIGNMENT_4_MEMORY_ALLOCATOR_OWN_TESTS_H
#define ASSIGNMENT_4_MEMORY_ALLOCATOR_OWN_TESTS_H

#include "mem.h"
#include "mem_internals.h"
#include "util.h"
#include <sys/mman.h>

bool default_success_allocation();

bool free_one_block_allocation();

bool free_two_blocks_allocation();

bool grow_heap_after_is_free();

bool grow_heap_after_is_not_free();

#endif //ASSIGNMENT_4_MEMORY_ALLOCATOR_OWN_TESTS_H
